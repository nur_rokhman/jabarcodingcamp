import 'dart:io';

void main(List<String> args) {
// NOMOR 1
  // var word = 'dart';
  // var second = 'is';
  // var third = 'awesome';
  // var fourth = 'and';
  // var fifth = 'I';
  // var sixth = 'love';
  // var seventh = 'it!';

  // print("$word $second $third $fourth $fifth $sixth $seventh");

// NOMOR 2
  // var sentence = "I am going to be Flutter Developer";
  // var exampleFirstWord = sentence[0] ;
  // var exampleSecondWord = sentence[2] + sentence[3] ;
  // var secondWord = exampleSecondWord;
  // var thirdWord = sentence[5] + sentence[6] + sentence[7] + sentence[8] + sentence[9]; // lakukan sendiri
  // var fourthWord = sentence[11] + sentence[12]; // lakukan sendiri
  // var fifthWord = sentence[14] + sentence[15]; // lakukan sendiri
  // var sixthWord = sentence[17] + sentence[18] + sentence[19] + sentence[20] + sentence[21] + sentence[22] + sentence[23]; // lakukan sendiri
  // var seventhWord = sentence[25] + sentence[26] + sentence[27] + sentence[28] + sentence[29] + sentence[30] + 
  //                   sentence[31] + sentence[32] + sentence[33]; // lakukan sendiri
  
  // print('First Word: ' + exampleFirstWord);
  // print('Second Word: ' + secondWord);
  // print('Third Word: ' + thirdWord);
  // print('Fourth Word: ' + fourthWord);
  // print('Fifth Word: ' + fifthWord);
  // print('Sixth Word: ' + sixthWord);
  // print('Seventh Word: ' + seventhWord);

//  NOMOR 3
  // print("masukan nama depan: ");
  // String? inputNama1 = stdin.readLineSync();
  // print("masukan nama belakang: ");
  // String? inputNama2 = stdin.readLineSync();
  // print("\nnama lengkap anda adalah:\n${inputNama1} ${inputNama2}");

// NOMOR 4
  int a = 5;
  int b = 10;
  var kali = a*b;
  var bagi = a/b;
  var jumlah = a+b;
  var kurang = a-b;

  print("perkalian: $kali");
  print("pembagian: $bagi");
  print("penjumlahan: $jumlah");
  print("pengurangan: $kurang");
}