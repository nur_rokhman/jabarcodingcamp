import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_firebase_login/app/app.dart';
import 'package:flutter_firebase_login/home/view/account_screen.dart';


class HomeScreen extends StatelessWidget {
  int _price(int index) {
    int? index = 0;
    index += index;
    return index;
  }

  @override
  Widget build(BuildContext context) {  
  final textTheme = Theme.of(context).textTheme;
  final user = context.select((AppBloc bloc) => bloc.state.user);
    return Container(
        decoration: BoxDecoration(
          // image: DecorationImage(
          //     image: AssetImage('assets/img/logo.png'), fit: BoxFit.cover),
          gradient: LinearGradient(
              colors: [Colors.blue.shade400, Colors.white],
              begin: Alignment.bottomCenter,
              end: Alignment.topCenter),
        ),
        child: Center(
          child: Column(
            children: <Widget>[
              SizedBox(
                height: 20,
              ),
              Container(
                height: 50,
                padding: EdgeInsets.only(right: 35),
                margin: EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Image(image: AssetImage('assets/notifications.png')),
                    SizedBox(
                      width: 20,
                    ),
                    Image(image: AssetImage('assets/add_shopping_cart.png')),
                    SizedBox(
                      width: 20,
                    ),
                  ],
                ),
              ),
              
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  SizedBox(
                    width: 20,
                  ),
                  Text(
                    'Welcome,',
                    style: TextStyle(
                        color: Colors.blue.shade400,
                        fontWeight: FontWeight.w700,
                        fontSize: 55),
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  SizedBox(
                    width: 35,
                  ),
                  Text(user.email ?? '', style: TextStyle(
                        color: Colors.blue.shade900,
                        fontSize: 35),
                  ),
                ],
              ),
              SizedBox(
                height: 20,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 35, right: 35),
              child: TextField(
                decoration: new InputDecoration(
                prefixIcon: Icon(Icons.search),
                        border: InputBorder.none,
                labelText: "Search",
                enabledBorder: const OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(20.0)),
                borderSide: const BorderSide(
                  color: Colors.blue,
                  ),
                ),
                focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(10.0)),
                  borderSide: BorderSide(color: Colors.blue, width: 2.5),
                ),
              ),
              ),
              ),
              
              SizedBox(
                height: 20,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  SizedBox(
                    width: 35,
                  ),
                  Text(
                    "UPDATE NEWS",
                    style: TextStyle(color: Colors.white, fontSize: 16),
                  ),
                  SizedBox(
                    width: 10,
                  ),
                ],
              ),
              SizedBox(
                    height: 10,
                  ),
              Container(
                height: 300,
                margin: EdgeInsets.all(8.0),
                child: GridView.count(

          crossAxisCount: 2,
          children: List.generate(100, (index) {
            return Center(
              child: Column(
                children: <Widget>[
                  Image.network(
                    'https://picsum.photos/500/500?random=${++index}',
                    width: 100,
                    height: 100,
                  ),
                  Text(
                    'Berita $index',
                    style: Theme.of(context).textTheme.headline5,
                  ),
                  FlatButton(
                        color: Colors.blue,
                        textColor: Colors.white,
                        padding: EdgeInsets.all(8.0),
                        splashColor: Colors.blueAccent,
                        onPressed: () {
                            Navigator.push(context,
                            MaterialPageRoute<AccountScreen>(builder: (ctx) => AccountScreen()));
                        },
                  child: Text(
                    "Beli",
                    style: TextStyle(fontSize: 20.0),
                  ),
                ),
                ],
              ),
            );
          }), 
          )
              ),
              SizedBox(
                height: 10,
              ),
            ],
          ),
        ),
      );
  }
}