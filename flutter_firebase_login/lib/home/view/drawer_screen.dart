import 'package:flutter/cupertino.dart'; 
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_firebase_login/app/app.dart';

class DrawerScreen extends StatefulWidget {
  @override
  _DrawwerScreenState createState() => _DrawwerScreenState();
}

class _DrawwerScreenState extends State<DrawerScreen> {
  @override 
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;
    final user = context.select((AppBloc bloc) => bloc.state.user);
    return Drawer(
      child: ListView(
        children: <Widget>[ 
          UserAccountsDrawerHeader(
            accountName: Text("Your Name"), 
            currentAccountPicture: CircleAvatar(
              backgroundImage: AssetImage("assets/omen.jpg")
            ),
            accountEmail: Text(user.email ?? '', style: TextStyle(
                        color: Colors.blue.shade900,
                        fontSize: 15),
                  ),
          ),
          DrawerListTile(
            iconData : Icons. group, 
            title: "NewGroup", 
            onTilePressed: (){},
          ),
            DrawerListTile( 
              iconData : Icons.lock, 
              title: "New Secret Group", 
              onTilePressed: (){}, 
          ), // DrawerListTile
            DrawerListTile( 
              iconData : Icons.notifications, 
              title: "New Channel Chat", 
              onTilePressed: (){},
          ),
            DrawerListTile( 
              iconData : Icons.contacts, 
              title: "contacts", 
              onTilePressed: (){},
          ), // DrawerListTile
            DrawerListTile( 
              iconData : Icons.bookmark_border, 
              title: "Saved Message", 
              onTilePressed: (){},
          ), // DrawerListTile
            DrawerListTile( 
              iconData : Icons.phone, 
              title: "Calls", 
              onTilePressed: (){},
          )// DrawerListTile
          ], 
        )
    );
  }
}

class DrawerListTile extends StatelessWidget {
  final IconData iconData; 
  final String title; 
  final VoidCallback onTilePressed;

  const DrawerListTile({Key? key, required this.iconData, required this.title, required this.onTilePressed})
  :super(key: key); 
  @override 
  Widget build(BuildContext context) {
    return ListTile(
    onTap: onTilePressed, 
    dense: true, 
    leading: Icon(iconData), 
    title: Text(title, style: TextStyle(fontSize: 16),),
    );
  }
}