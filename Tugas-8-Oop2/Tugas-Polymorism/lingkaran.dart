import 'bangun_datar.dart';
import 'dart:math';

class Lingkaran extends BangunDatar{
  double? r;
  double PI = pi;

  Lingkaran(double r){
    this.r = r;
  }

  @override
  double luas(){
    double luas = PI * pow(r!, 2);
    return luas;
  }
  @override
  double keliling(){
    double keliling = 2 * PI * r!;
    return keliling;
  }
}