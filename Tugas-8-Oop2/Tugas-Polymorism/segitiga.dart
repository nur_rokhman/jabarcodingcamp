import 'bangun_datar.dart';

class Segitiga extends BangunDatar{
  double? alas; 
  double? tinggi;
  double? miring;

  Segitiga(double alas, double tinggi, double miring){
    this.alas = alas;
    this.tinggi = tinggi;
    this.miring = miring;
  }

  @override
  double luas(){
    double luas = 0.5 * alas! * tinggi!;
    return luas;
  }
  @override
  double keliling(){
    double keliling = alas! + miring! + tinggi!;
    return keliling;
  }
}