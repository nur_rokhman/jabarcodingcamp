import 'bangun_datar.dart';

class Persegi extends BangunDatar{
  double? sisi;
  Persegi(double sisi){
    this.sisi = sisi;
  }

  @override
  double luas(){
    double luas = sisi! * sisi!;
    return luas;
  }
  @override
  double keliling(){
    double keliling = 4 * sisi!;
    return keliling;
  }
}