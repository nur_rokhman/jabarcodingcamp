import 'bangun_datar.dart';
import 'persegi.dart';
import 'segitiga.dart';
import 'lingkaran.dart';

void main(List<String> args) {
  BangunDatar bgnDatar = new BangunDatar();
  Persegi persegi = new Persegi(4);
  Segitiga segitiga = new Segitiga(3, 4, 5);
  Lingkaran lingkaran = new Lingkaran(4);

  bgnDatar.luas();
  print("Luas Persegi: ${persegi.luas()}");
  print("Luas segitiga: ${segitiga.luas()}");
  print("Luas lingkaran: ${lingkaran.luas().toStringAsFixed(2)}");
  print("");
  bgnDatar.keliling();
  print("Luas Persegi: ${persegi.keliling()}");
  print("Luas segitiga: ${segitiga.keliling()}");
  print("Luas lingkaran: ${lingkaran.keliling().toStringAsFixed(2)}");
}