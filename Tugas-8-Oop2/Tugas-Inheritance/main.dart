import 'human.dart';
import 'beast_titan.dart';
import 'armor_titan.dart';
import 'attack_titan.dart';

void main(List<String> args) {
  ArmorTitan armor = ArmorTitan();
  AttackTitan attack = AttackTitan();
  BeastTitan beast = BeastTitan();
  Human human = Human();

  armor.powerPoint = 8;
  attack.powerPoint = 10;
  beast.powerPoint = 8;
  human.powerPoint = 10;

  print("power point Armor Titan: ${armor.powerPoint}");
  print("power point Attack Titan: ${attack.powerPoint}");
  print("power point Beast Titan: ${beast.powerPoint}");
  print("power point Human: ${human.powerPoint}");
  print("");
  print("sifat dari Armor Titan: ${armor.terjang()}");
  print("sifat dari Attack Titan: ${attack.punch()}");
  print("sifat dari Beast Titan: ${beast.lempar()}");
  print("sifat dari Human: ${human.killAlltitan()}");
}