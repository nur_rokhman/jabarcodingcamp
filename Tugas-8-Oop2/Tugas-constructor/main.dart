import 'employee.dart';

void main(List<String> args) {
  var idEmployee = new Employee.id(119);
  var nameEmployee = new Employee.name("Nur Rokhman");
  var deptEmployee = new Employee.departement("Chief Marketing Office");

  print(idEmployee.id);
  print(nameEmployee.name);
  print(deptEmployee.departement);
}