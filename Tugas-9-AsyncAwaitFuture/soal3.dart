void main(List<String> args) async{
  print("Ready. Sing");
  print(await line());
  print(await line2());
  print(await line3());
  print(await line4());
}

Future<String> line() async{
  String nyanyi = "pernahkah kau merasa....";
  return await Future.delayed(Duration(seconds: 5), () => (nyanyi));
}

Future<String> line2() async{
  String nyanyi = "pernahkah kau merasa........";
  return await Future.delayed(Duration(seconds: 3), () => (nyanyi));
}

Future<String> line3() async{
  String nyanyi = "pernahkah kau merasa";
  return await Future.delayed(Duration(seconds: 2), () => (nyanyi));
}

Future<String> line4() async{
  String nyanyi = "Hatimu hampa, pernahkah kau merasa hatimu kosong.....";
  return await Future.delayed(Duration(seconds: 1), () => (nyanyi));
}