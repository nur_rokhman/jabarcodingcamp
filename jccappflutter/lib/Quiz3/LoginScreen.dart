// import 'dart:ui';

import 'package:flutter/material.dart';
// import 'customtextfield.dart';
import 'HomeScreen.dart';

class LoginScreen extends StatelessWidget {
  final myUsernameController = TextEditingController();
  final myPasswordController = TextEditingController();
  String? nUsername, nPassword;

  final _formKey = GlobalKey<FormState>();

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  void _showScaffold() {
    _scaffoldKey.currentState!.showSnackBar(SnackBar(
    content: Text('Password yang anda masukkan salah!'),
    behavior: SnackBarBehavior.floating,
    margin: EdgeInsets.all(30.0),
  ));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Form(
        key: _formKey,
        child: Container(
          child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            SizedBox(
              height: 50,
            ),
            TextFormField(
              //cek data field nya kosong
              validator: (value) {
                if (value!.isEmpty) {
                  return 'Please Input Username';
                }
                return null;
              },

              controller: myUsernameController,
              decoration: InputDecoration(
                hintText: 'Input Username',
              ),
            ),
            TextFormField(
              //cek data field nya kosong
              validator: (value) {
                if (value!.isEmpty) {
                  return 'Please Input Username';
                }
                return null;
              },
              maxLength: 20,
              maxLengthEnforced: true,
              controller: myPasswordController,
              obscureText: true,
              decoration: InputDecoration(
                hintText: 'Input Password',
              ),
            ),
            SizedBox(
              height: 25.0,
            ),
            MaterialButton(
              minWidth: 85.0,
              height: 50.0,
              color: Colors.green,
              textColor: Colors.white,
              onPressed: () {
                nUsername = myUsernameController.text;
                nPassword = myPasswordController.text;

                if (_formKey.currentState!.validate()) {

                  if(nPassword != 'jabarcodingcamp123'){
                     _showScaffold();
                  }else{
                    //aksi pindah
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => HomeScreen(
                              nama: nUsername,
                              password:
                              nPassword,
                            )));
                    }
                }
              },
              child: Text('Submit'),
            )
              
          ],
        ),
        )
      ),
    );
  }
}