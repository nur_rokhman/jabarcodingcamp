import 'package:flutter/material.dart';

class CustomTextField extends StatelessWidget {
  String? hint;
  bool? issecured;

  CustomTextField({this.hint, this.issecured});

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.only(left: 35, right: 35),
        child: TextField(obscureText: issecured!,
          cursorColor: Colors.white,
          style: TextStyle(color: Colors.white),
          decoration: InputDecoration(
              disabledBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.blue),
                borderRadius: BorderRadius.circular(10),
              ),
              hintText: hint,
              hintStyle: TextStyle(
                  fontSize: 16,
                  letterSpacing: 1.5,
                  color: Colors.white70,
                  fontWeight: FontWeight.w900),
              filled: true,
              hoverColor: Colors.green,
              focusColor: Colors.green,
              fillColor: Colors.white.withOpacity(.3),
              border: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.green),
                borderRadius: BorderRadius.circular(10),
              ),
              enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.blue, width: 2.5),
                borderRadius: BorderRadius.circular(10),
              )),
        ));
  }
}