import 'package:flutter/material.dart';

class HomeScreen extends StatefulWidget {
  final String? nama;
  final String? password;

  HomeScreen({Key? key, this.nama, this.password}) : super(key:  key);

  @override
  _HomeScreen createState() => _HomeScreen();
}

class _HomeScreen extends State<HomeScreen> {
  int _price(int index) {
    int? index = 0;
    index += index;
    return index;
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Container(
        
        decoration: BoxDecoration(
          // image: DecorationImage(
          //     image: AssetImage('assets/img/logo.png'), fit: BoxFit.cover),
          gradient: LinearGradient(
              colors: [Colors.blue.shade400, Colors.white],
              begin: Alignment.bottomCenter,
              end: Alignment.topCenter),
        ),
        child: Center(
          child: Column(
            children: <Widget>[
              SizedBox(
                height: 80,
              ),
              Container(
                height: 50,
                padding: EdgeInsets.only(right: 35),
                margin: EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Text('price: ${_price(2)}'),
                    SizedBox(
                      width: 20,
                    ),
                    Image(image: AssetImage('assets/img/notifications.png')),
                    SizedBox(
                      width: 20,
                    ),
                    Image(image: AssetImage('assets/img/add_shopping_cart.png'))
                  ],
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  SizedBox(
                    width: 35,
                  ),
                  Text(
                    'Welcome,',
                    style: TextStyle(
                        color: Colors.blue.shade400,
                        fontWeight: FontWeight.w700,
                        fontSize: 55),
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  SizedBox(
                    width: 35,
                  ),
                  Text(
                    '${widget.nama}',
                    style: TextStyle(
                        color: Colors.blue.shade900,
                        fontSize: 35),
                  ),
                ],
              ),
              SizedBox(
                height: 68,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 35, right: 35),
              child: TextField(
                decoration: new InputDecoration(
                prefixIcon: Icon(Icons.search),
                        border: InputBorder.none,
                labelText: "Search",
                enabledBorder: const OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(20.0)),
                borderSide: const BorderSide(
                  color: Colors.blue,
                  ),
                ),
                focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(10.0)),
                  borderSide: BorderSide(color: Colors.blue, width: 2.5),
                ),
              ),
              ),
              ),
              
              SizedBox(
                height: 72,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  SizedBox(
                    width: 35,
                  ),
                  Text(
                    "Recomended Place",
                    style: TextStyle(color: Colors.white, fontSize: 16),
                  ),
                  SizedBox(
                    width: 10,
                  ),
                ],
              ),
              SizedBox(
                    height: 10,
                  ),
              Container(
                height: 900,
                margin: EdgeInsets.all(8.0),
                child: GridView.count(

          crossAxisCount: 2,
          children: List.generate(100, (index) {
            return Center(
              child: Column(
                children: <Widget>[
                  Image.network(
                    'https://picsum.photos/500/500?random=${++index}',
                    width: 100,
                    height: 100,
                  ),
                  Text(
                    'Rp. $index',
                    style: Theme.of(context).textTheme.headline5,
                  ),
                  FlatButton(
                        color: Colors.blue,
                        textColor: Colors.white,
                        padding: EdgeInsets.all(8.0),
                        splashColor: Colors.blueAccent,
                        onPressed: () {
                            _price(index);
                        },
                  child: Text(
                    "Beli",
                    style: TextStyle(fontSize: 20.0),
                  ),
                ),
                ],
              ),
            );
          }), 
          )
              ),
              SizedBox(
                height: 10,
              ),
              
            ],
          ),
        ),
      ),
    );
  }
}