import 'dart:convert';
import 'dart:developer';

import 'package:flutter/material.dart'; 
import 'package:get/get.dart'; 
import 'Latihan/Latihan17/add-news.dart'; 
import 'package:http/http.dart' as http;

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  
  @override 
  Widget build(BuildContext context){
    return GetMaterialApp(
      title: 'GetX Http Post', 
      theme: ThemeData(
        primarySwatch: Colors. blue,
    ), // ThemeData 
    home: MyHomePage(title: 'GetX Http Post'),
    ); // GetMaterialApp
  }   
}
  
class MyHomePage extends StatefulWidget {
    MyHomePage({Key? key, required this.title}) : super(key: key);
    
    final String title;
    @override
    MyHomePageState createState() => MyHomePageState();
}

class MyHomePageState extends State<MyHomePage> { 
  final controller = Get.put(Controller());
  
  Future<void> getNews() async {
    var url = Uri.parse('https://achmadhilmy-sanbercode.my.id/api/v1/news'); 
    var response = await http.get(url); 
    var news = jsonDecode(response.body); 
    controller.setNews(news["data"]);
  }
  
  Future<void> addNews(String title, String value) async { 
    var url = Uri.parse('https://achmadhilmy-sanbercode.my.id/api/v1/news'); 
    await http.post(url, body: {'title': title, 'value': value}); 
    getNews();
  }
    
  @override 
  void initState() {
    // TODO: implement initState 
    getNews(); 
    super.initState();
  }

  @override 
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar( 
        title: Text(widget.title),
      ), // AppBar 
      body: Obx(
        () => controller.news.length > 0
        ? ListView.builder(
          itemCount: controller.news.length, 
          itemBuilder: (BuildContext context, int index) {
            return ListTile(
            title: Text(controller.news[index]["title"]),
            subtitle: Text(controller.news[index]["value"]),
       ); // ListTile
      },
    )// ListView. builder
    : const Center(child: Text('Berita Tidak Ada')),
  ), // Obx 
      
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          var berita = await Navigator. push(
            context, MaterialPageRoute(builder: (context) => AddNews())); 
            addNews(berita["title"], berita["value"]);
        },
            tooltip: 'getNews', 
            child: Icon(Icons. plus_one_rounded) // FloatingActionButton
        ),
   );
   }
 } // Scaffold

 class Controller extends GetxController {
  RxList news = [].obs; 
  
  void setNews(List _val){
  news. value = _val;
  }
}