import 'dart:convert'; 
import 'package:flutter/cupertino.dart'; 
import 'package:flutter/material.dart'; 
import 'package:http/http.dart' as http; 
import 'get_detail.dart';
import 'post_data.dart';

class GetDataScreen extends StatefulWidget {
  const GetDataScreen({Key? key}) : super(key: key);
  
  @override
  _GetDataScreenState createState() => _GetDataScreenState();
}
  
class _GetDataScreenState extends State<GetDataScreen> {
  final String url = "https://achmadhilmy-sanbercode.my.id/api/v1/news"; 
  List? data; 
  
  @override 
  void initState() {
    _getRefreshData(); 
    super.initState();
  }

  Future<void> _getRefreshData() async {
    this.getJsonData(context);
  }

  Future<String?> getJsonData(BuildContext context) async { 
    var response =
      await http.get(Uri.parse(url), headers: {"Accept": "application/json"});
    print(response. body); 
    setState(() {
      var convertDataToJson = jsonDecode(response.body); 
      data = convertDataToJson['data'];
    });
  }

  @override
   Widget build(BuildContext context) {
      return Scaffold(
      appBar: AppBar(title: Text("Get Data Api ")), 
      body: RefreshIndicator(
        onRefresh: _getRefreshData, 
        child: ListView.builder(
          itemCount: data == null ? 0 : data!.length, 
          itemBuilder: (BuildContext context, int index) {
            return Container(
              margin: EdgeInsets.all(5.0), 
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch, 
                children: [
                  GestureDetector(
                    onTap: () { 
                      var data1 = data! [index]['title']; 
                      var data2 = data! [index]['value']; 
                      Navigator. push(
                        context, 
                        MaterialPageRoute(
                          builder: (context) =>
                            getDetailScreen(value: [data1, data2])));                     },
                    child: Padding(
                      padding: const EdgeInsets.all(16.0), 
                      child: Column(
                        //mainAxisAlignment: MainAxisAlignment.center, 
                        crossAxisAlignment: CrossAxisAlignment.start, 
                        children: <Widget>[
                          SizedBox(
                          height: 5,
                          ), // SizedBox 
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween, 
                            crossAxisAlignment: CrossAxisAlignment.center, 
                            children: [
                              Text(
                              data! [index]['title']!,

                              // data! [index]['nama_kegiatan'], 
                              style: TextStyle( 
                                fontSize: 14, 
                                fontWeight: FontWeight.w700), // TextStyle
                              ), // Text 
                              Icon(Icons. chevron_right),
                              ],
                              ), // Row
                              // Text(filterBantuanList // 
                              SizedBox(
                                height: 3,
                              ),// SizedBox 
                              Divider(
                                height: 20, 
                                thickness: 1, 
                                endIndent: 25, 
                                indent: 15, // Divider
                              )
                              ],
                      ),
                    ),
                  )
                ],
              ),
            );
          }),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator. push(
            context, MaterialPageRoute(builder: (context) => PostDataApi()));
        },
        child: Icon(Icons. add),
        ), // FloatingActionButton
        ); // Scaffold
   }
}