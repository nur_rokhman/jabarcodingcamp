import 'dart:ui';

import 'package:flutter/material.dart';
import 'customwidget/customtextfield.dart';
import 'HomeScreen.dart';

class LoginScreen extends StatelessWidget {
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Container(
        decoration: BoxDecoration(
          // image: DecorationImage(
          //     image: AssetImage('assets/img/logo.png'), fit: BoxFit.cover),
          gradient: LinearGradient(
              colors: [Colors.blue.shade400, Colors.white],
              begin: Alignment.bottomCenter,
              end: Alignment.topCenter),
        ),
        child: Center(
          child: Column(
            children: <Widget>[
              SizedBox(
                height: 100,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    'Sanber Flutter',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: Colors.blue.shade400,
                        fontWeight: FontWeight.bold,
                        fontSize: 35),
                  ),
                ],
              ),
              SizedBox(
                height: 10,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  // SizedBox(
                  //   width: 40,
                  // ),
                  Image(image: AssetImage('assets/img/logo.png'), height: 120.0),
                    // SizedBox(
                    //   width: 20,
                    // ),
                ],
              ),
              SizedBox(
                height: 20,
              ),
              TextField(
                decoration: InputDecoration(
                  border: OutlineInputBorder(), labelText: "Username"
                ),
                  controller: emailController,
              ),  
              SizedBox(
                height: 18.5,
              ),
              TextField(
                decoration: InputDecoration(
                  border: OutlineInputBorder(), labelText: "Password",
                ),
                  controller: passwordController,
                  obscureText: true
              ),
              SizedBox(
                height: 18.5,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  GestureDetector(
                    onTap: () {},
                    child: Container(
                      child: Text(
                        'Forgot Password?',
                        style: TextStyle(
                            color: Colors.white, fontWeight: FontWeight.w700, fontSize: 12),
                      ),
                    ),
                  ),
                  // SizedBox(
                  //   width: 40,
                  // )
                ],
              ),
              SizedBox(
                height: 10,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 35, right: 35),
                child: ButtonTheme(
                    buttonColor: Colors.white,
                    minWidth: MediaQuery.of(context).size.width,
                    height: 45,
                    child: RaisedButton(
                      onPressed: () {
                        Navigator.push(context,
                          MaterialPageRoute(builder: (ctx) => HomeScreen()));
                      },
                      child: Text(
                        'Login',
                        style: TextStyle(color: Colors.blue.shade400, fontSize: 22),
                      ),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10)),
                    )),
              ),
              SizedBox(
                height: 20,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 35, right: 35),
                child: ButtonTheme(
                    buttonColor: Colors.white,
                    minWidth: MediaQuery.of(context).size.width,
                    height: 45,
                    child: RaisedButton(
                      onPressed: () {
                        Navigator.push(context,
                          MaterialPageRoute(builder: (ctx) => HomeScreen()));
                      },
                      child: Text(
                        'Register',
                        style: TextStyle(color: Colors.blue.shade400, fontSize: 22),
                      ),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10)),
                    )),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    "Don't Have an Accout ?",
                    style: TextStyle(color: Colors.white),
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  GestureDetector(
                    onTap: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (ctx) => HomeScreen()));
                    },
                    child: Text(
                      'Sign in',
                      style: TextStyle(
                          decoration: TextDecoration.underline,
                          fontWeight: FontWeight.w500,
                          color: Colors.white),
                    ),)
                ],
              ),
              SizedBox(
                    height: 70,
                  ),
              Container(
                height: 100,
                
                margin: EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Image(image: AssetImage('assets/img/monas.png')),
                    SizedBox(
                      width: 20,
                    ),
                    Image(image: AssetImage('assets/img/berlin.png'))
                  ],
                ),
              ),
              SizedBox(
                height: 25,
              ),
            ],
          ),
        ),
      ),
    );
  }
}