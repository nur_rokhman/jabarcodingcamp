import 'package:flutter/material.dart';
import 'AccountScreen.dart';
import 'HomeScreen.dart';
import 'DrawerScreen.dart';
import 'SearchScreen.dart';
 
class BottomNavBar extends StatefulWidget {
  BottomNavBar() : super();
 
  final String title = "Flutter Bottom Tab";
 
  @override
  _BottomNavBar createState() => _BottomNavBar();
}
 
class _BottomNavBar extends State<BottomNavBar> {
  int currentTabIndex = 0;
  List<Widget> tabs = [
    HomeScreen(),
    SearchScreen(),
    AccountScreen(),
  ];
  onTapped(int index) {
    setState(() {
      currentTabIndex = index;
    });
  }
 
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("JCC Forum"), 
        actions: <Widget>[ 
          Padding(padding: const EdgeInsets.all(8.0), 
          child: Icon(Icons.search),
         ) // Padding
      ],
      ),
      drawer: DrawerScreen(),
      body: tabs[currentTabIndex],
      bottomNavigationBar: BottomNavigationBar(
        onTap: onTapped,
        currentIndex: currentTabIndex,
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            title: Text("Home"),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.search),
            title: Text("Search"),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.person),
            title: Text("Profile"),
          )
        ],
      ),
    );
  }
}