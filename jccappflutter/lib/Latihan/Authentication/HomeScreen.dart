import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'LoginScreen.dart';

class HomeScreen extends StatelessWidget {
  static String tag = 'home-page';
  Future<void> signOut() async {
    await FirebaseAuth.instance.signOut();
  }

  @override
  Widget build(BuildContext context) {
    FirebaseAuth auth = FirebaseAuth.instance;
    if(auth.currentUser != null){
      print(auth.currentUser!.email);
    }
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Container(
        decoration: BoxDecoration(
          // image: DecorationImage(
          //     image: AssetImage('assets/img/logo.png'), fit: BoxFit.cover),
          gradient: LinearGradient(
              colors: [Colors.blue.shade400, Colors.white],
              begin: Alignment.bottomCenter,
              end: Alignment.topCenter),
        ),
        child: Center(
          child: Column(
            children: <Widget>[
              SizedBox(
                height: 80,
              ),
              Container(
                height: 50,
                padding: EdgeInsets.only(right: 35),
                margin: EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Image(image: AssetImage('assets/img/notifications.png')),
                    SizedBox(
                      width: 20,
                    ),
                    Image(image: AssetImage('assets/img/add_shopping_cart.png')),
                    SizedBox(
                      width: 20,
                    ),
                    Container(
                      child: ElevatedButton(onPressed: () {
                      signOut().then((value) => Navigator.of(context)
                      .pushReplacement(MaterialPageRoute(builder:(context)=>
                      LoginScreen()
                      ))
                      );
                      }, child: Text('logout'),
                      ),
                    )
                  ],
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  SizedBox(
                    width: 35,
                  ),
                  Text(
                    'Welcome,',
                    style: TextStyle(
                        color: Colors.blue.shade400,
                        fontWeight: FontWeight.w700,
                        fontSize: 55),
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  SizedBox(
                    width: 35,
                  ),
                  Text(
                    '${auth.currentUser!.email}',
                    style: TextStyle(
                        color: Colors.blue.shade900,
                        fontSize: 35),
                  ),
                ],
              ),
              SizedBox(
                height: 68,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 35, right: 35),
              child: TextField(
                decoration: new InputDecoration(
                prefixIcon: Icon(Icons.search),
                        border: InputBorder.none,
                labelText: "Search",
                enabledBorder: const OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(20.0)),
                borderSide: const BorderSide(
                  color: Colors.blue,
                  ),
                ),
                focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(10.0)),
                  borderSide: BorderSide(color: Colors.blue, width: 2.5),
                ),
              ),
              ),
              ),
              
              SizedBox(
                height: 72,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  SizedBox(
                    width: 35,
                  ),
                  Text(
                    "Recomended Place",
                    style: TextStyle(color: Colors.white, fontSize: 16),
                  ),
                  SizedBox(
                    width: 10,
                  ),
                ],
              ),
              SizedBox(
                    height: 10,
                  ),
              Container(
                height: 100,
                margin: EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(
                    width: 10,
                  ),
                    Image(image: AssetImage('assets/img/monas.png')),
                    SizedBox(
                      width: 20,
                    ),
                    Image(image: AssetImage('assets/img/berlin.png'))
                  ],
                ),
              ),
              SizedBox(
                    height: 10,
                  ),
              Container(
                height: 100,
                margin: EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(
                    width: 10,
                  ),
                    Image(image: AssetImage('assets/img/roma.png')),
                    SizedBox(
                      width: 20,
                    ),
                    Image(image: AssetImage('assets/img/tokyo.png'))
                  ],
                ),
              ),
              SizedBox(
                height: 10,
              ),
              
            ],
          ),
        ),
      ),
    );
  }
}