import 'package:flutter/material.dart'; 

class AddNews extends StatefulWidget {
  AddNews({Key? key}) : super(key: key);
  
  @override
  _AddNewsState createState() => _AddNewsState();
}
  
class _AddNewsState extends State<AddNews> {
  TextEditingController titleController = TextEditingController(); 
  TextEditingController valueController = TextEditingController();
  
  @override Widget build(BuildContext context) {
    return MaterialApp(
      title: 'GetX Http Post', 
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ), // ThemeData 
      home: Scaffold(
        appBar: AppBar(
          title: Text("Tambah Berita"),
        ), // AppBar 
        body: SingleChildScrollView(
          child: Container( 
            child: Padding(
              padding: const EdgeInsets.all(8.0), 
              child: Column(
                children: [
                TextFormField(
                  controller: titleController,
                  decoration: const InputDecoration(
                    border: UnderlineInputBorder(), labelText: 'Title'), // InputDecoration
                  ), // TextFormField 
                  TextFormField(
                    controller: valueController, 
                    decoration: const InputDecoration(
                      border: UnderlineInputBorder(), labelText: 'Value'), // InputDecoration
                  ), // TextFormField 
                  ElevatedButton(
                    child: Text('Publis Berita'), 
                    onPressed: () {
                      Navigator.pop(context, {"title": titleController.text, "value": valueController.text});
                  },
                  )
                ],
              ),
            ), 
          ),
        ),
      ),
    );
  }
}