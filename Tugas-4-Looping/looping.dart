import 'dart:io';

void main(List<String> args) {
// NOMOR 1
    // var flag = 0;
    // print("LOOPING PERTAMA");
    // while(flag < 20) { 
    //   flag += 2; // Mengubah nilai flag dengan menambahkan 2
    //   print ("$flag - I Love Coding"); 
    //   }

    // print("LOOPING KEDUA");
    // while(flag > 2) { 
    //   flag -= 2; // Mengubah nilai flag dengan mengurangi 2
    //   print ("$flag - I will become fullstack developer"); 
    //   }

// NOMOR 2
    // for(var deret = 1; deret <= 20; deret++){
    //   if((deret%2) == 1 && (deret%3) != 0){
    //       print("$deret - santai");
    //   } else if((deret%2) != 1){
    //       print("$deret - berkualitas");
    //   } else {
    //       print("$deret - I Love Coding");
    //   }
    // }

// NOMOR 3 - For Loop
    // var persegi = "#";
    // for(var tinggi = 1; tinggi <= 4; tinggi++){
    //   for(var lebar = 1; lebar <= 8; lebar++){
    //     stdout.write(persegi);
    //   }
    //   print("");
    // }

// NOMOR 3 - While Loop
    // var persegi = "#";
    // var tinggi = 1;
    // while(tinggi <= 4){
    //   var lebar = 1;
    //   while(lebar <= 8){
    //     stdout.write(persegi);
    //     lebar++;
    //   }
    //   print("");
    //   tinggi++;
    // }

// NOMOR 3 - DO WHILE
    // var persegi = "#";
    // var tinggi = 1;
    // do{
    //   var lebar = 1;
    //   do{
    //     stdout.write(persegi);
    //     lebar++;
    //   } while(lebar <= 8);
    //     print("");
    //     tinggi++;
    // } while(tinggi <= 4);

// NOMOR 4 - FOR LOOP
    // var dimensi = 7;     
    // for(var y=1; y<=dimensi; y++) {
    //     for(var x=0; x<y; x++) {
    //         stdout.write('#');
    //     }
    //     print('');
    // }

// NOMOR 4 - WHILE LOOP
    // var dimensi = 7;
    // var y = 1;
    // while(y <= dimensi){
    //    var x = 0;
    //   while(x < y){
    //     stdout.write("#");
    //     x++;
    //   }
    //   print("");
    //   y++;
    // }

// NOMOR 4 - DO WHILE
    var dimensi = 7;
    var y = 1;
    do{
      var x = 0;
      do{
        stdout.write("#");
        x++;
      } while(x < y);
        print("");
        y++;
    } while(y <= dimensi);
}