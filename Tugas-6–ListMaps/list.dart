// NOMOR 1 - RANGE
  // void main(List<String> args) {
  //   print(range(1, 10)); //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
  //   print(range(11,18)); // [11, 12, 13, 14, 15, 16, 17, 18]

  //   print(range(54, 50)); // [54, 53, 52, 51, 50] 
  // }

  // range(startNum, finishNum){
  //   List list = [];
  //   var i;
  //   if (startNum < finishNum) {
  //     for (i = startNum; i <= finishNum; i++){
  //       list.add(i);
  //     }
  //   } else {
  //     for (i = startNum; i >= finishNum; i--){
  //       list.add(i);
  //     }
  //   }
  //   return list;
  // }

// NOMOR 2 - RANGE WITH STEP
  // void main(List<String> args) {
  //   print(rangeWithStep(1, 10, 2)); // [1, 3, 5, 7, 9]
  //   print(rangeWithStep(11, 23, 3)); // [11, 14, 17, 20, 23]
  //   print(rangeWithStep(5, 2, 1)); // [5, 4, 3, 2]
  //   }

  //   rangeWithStep(startNum, finishNum, step){
  //     List list = [];
  //     var i;
  //     if (startNum < finishNum) {
  //       for (i = startNum; i <= finishNum; i+=step){
  //         list.add(i);
  //       }
  //     } else {
  //       for (i = startNum; i >= finishNum; i-=step){
  //         list.add(i);
  //       }
  //     }
  //     return list;
  //   }

  // NOMOR 3 - LIST MULTIDIMENSI
    //contoh input
    // void main(List<String> args) {
    //   var input = [
    //             ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    //             ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    //             ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    //             ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
    //         ] ;
      
    //   dataHandling(input);
    // }

    // dataHandling(inputs) {
    //   for(var i = 0; i <= 4; i++){
    //     print("Nomor ID: " + inputs[i][0] + "\nNama Lengkap: " + inputs[i][1] + "\nTTL: " +
    //     inputs[i][2] + " " + inputs[i][3] + "\nHobi: " + inputs[i][4] + "\n");
    //   }
    //   return 0;
    // }

// NOMOR 4 - Balik Kata
void main(List<String> args) {
  print(balikKata("Kasur")); // rusaK
  print(balikKata("SanberCode")); // edoCrebnaS
  print(balikKata("Haji")); // hajI
  print(balikKata("racecar")); // racecar
  print(balikKata("Sanbers")); // srebnaS 
}

balikKata(s){
    String rev="";
    for(int j=s.length();j>0;--j)
    {
    rev=rev+(s.charAt(j-1)); 
    }
    return rev;
}