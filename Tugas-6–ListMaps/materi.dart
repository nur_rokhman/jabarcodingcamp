void main() {
// contoh LIST
  // List <int> list = [23, 24, 25];
  // list.add(0);    
  // print(list[0]);
  // print(list[1]);
  // print(list[2]);
  // print(list[3]);

// Contoh MAP
  // Map<String, String> kota = {'jkt' : 'Jakarta', 'bdg' : 'Bandung', 'sby' : 'Surabaya'};
         
  // print(kota['jkt']);
  // print(kota['bdg']);
  // print(kota['sby']);

// contoh 3
  // var languages = ["C", "C++", "Java", "Dart", "Javascript", 2 , 34];
  // for(var language in languages){    
  //   print(language);  
  // }
  // print("Total bahasa: ${languages.length}");

// Contoh 4
  // var arrayMulti = [ 
  //   [1, 2, 3],
  //   [4, 5, 6],
  //   [7, 8, 9]
  // ];
  // print(arrayMulti[0][0]); // 1 
  // print(arrayMulti[1][0]); // 4
  // print(arrayMulti[2][1]); // 8

// Contoh 5 - for each
  // var perusahaan = ['bukalapak', 'tokopedia', 'blibli'];
  // perusahaan.forEach((data)=> print(data));
// contoh 5 - map()
  // var perusahaan = ['bukalapak', 'tokopedia', 'blibli'];
  // var perusahan_id = perusahaan.map((data)=> print('${data} indonesia')).toList();
// Contoh 5 - contain()
  // var perusahaan = ['bukalapak', 'tokopedia', 'blibli', 'salestock'];
  // print(perusahaan.contains('bukalapak'));
// contoh 5 - sort()
  // var randomdata = [1,3,5,20,4,2];
  // randomdata.sort((a, b)=> a.compareTo(b));
  // print(randomdata);
// contoh 5 - reduce() fold()
  // var randomdata = [1,3,5,20,4,2];
  // var sumData = randomdata.reduce((cur, next)=> cur + next);
  // print(sumData); /// 35
  // const currentValue = 10;
  // var nextSum = randomdata.fold(currentValue, (cur, next)=> cur + next);
  // print(nextSum); // 45
// contoh 5 - every()
  // List<Map<String, dynamic>> listUser = [
  // {'nama': 'bekasi', 'umur': 240},
  // {'nama': 'boyolali', 'umur': 200},
  // {'nama': 'jakarta', 'umur': 100},
  // {'nama': 'surabaya', 'umur': 100},
  // ];
  // var example = listUser.every((data) => data['umur'] >= 100);
  // print(example); ///true
// contoh 5 - where(), firstWhere(), singleWhere()
  // List<Map<String, dynamic>> listUser = [
  // {'nama': 'bekasi', 'umur': 240},
  // {'nama': 'boyolali', 'umur': 200},
  // {'nama': 'jakarta', 'umur': 100},
  // {'nama': 'surabaya', 'umur': 100},
  // ];
  // var userYoung = listUser.where((data)=> data['umur'] > 100);
  // print(userYoung);
  
  // var userFirstYoung = listUser.firstWhere((data)=> data['umur'] < 200);
  // print(userFirstYoung); /// {‘nama’: ‘jakarta’, ‘umur’: 100},
  
  // var userSingle = listUser.singleWhere((data)=> data['umur'] <= 100);
  // print(userSingle); /// error karena ada dua kondisi yang benar
// contoh 5 - take(), skip()
  // var dataTestCase = [1, 2, 3, 4, 10, 90];
  // print(dataTestCase.take(2)); /// (1, 2)
  // print(dataTestCase.skip(2)); /// (3, 4, 10, 90)
// contoh 5 - expand
  // var pairs = [[1, 2], ['a', 'b'], [3, 4]];
  // var flatmaps = pairs.expand((pair)=> pair);
  // print(flatmaps);
// contoh 5 - List Comprehensions
  // var comph = [1,2,3,4];
  // var newCom = [for(var a in comph) 'new ${a}'];
  
  List<int> myList = [];
  List<int> list = [1,2,3];
  myList.add(1);
  myList.addAll(list);
  myList.forEach((bilangan)=>{
    print(bilangan)
});
}