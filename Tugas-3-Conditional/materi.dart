void main(List<String> args) {
  // Contoh 1
        // var isThisWahyu = true;
    
        // if(isThisWahyu){
        //     print("wahyu");
        // }else{
        //     print("bukan");
        // }

  // contoh 2
        // var isThisWahyu = true;
        // isThisWahyu ? print("wahyu") : print("bukan");

  // contoh 3
        //  if ( true) {
        //     print("jalankan code");
        //  }
  
  // contoh 4
        // if ( false ) {
        //   print("Program tidak jalan code");
        // }
  
  // contoh 5
        // var mood = "happy";
        // if ( mood == "happy" ) {
        //   print("hari ini aku bahagia!");
        // }

  // contoh 6
        // var minimarketStatus = "open";
        // if (minimarketStatus == "open") {
        //   print("saya akan membeli telur dan buah");
        // } else {
        //   print("minimarketnya tutup");
        // }

  // contoh 7
        // var minimarketStatus = "close";
        // var minuteRemainingToOpen = 5;
        // if (minimarketStatus == "open") {
        //   print("saya akan membeli telur dan buah");
        // } else if (minuteRemainingToOpen <= 5) {
        //   print("minimarket buka sebentar lagi, saya tungguin");
        // } else {
        //   print("minimarket tutup, saya pulang lagi");
        // }

  // contoh 7
        // var minimarketStatus = "open";
        // var telur = "soldout";
        // var buah = "soldout";
        // if (minimarketStatus == "open") {
        //   print("saya akan membeli telur dan buah");
        //   if (telur == "soldout" || buah == "soldout") {
        //     print("belanjaan saya tidak lengkap");
        //   } else if (telur == "soldout") {
        //     print("telur habis");
        //   } else if (buah == "soldout") {
        //     print("buah habis");
        //   }
        // } else {
        //   print("minimarket tutup, saya pulang lagi");
        // }

  // contoh 8
        var buttonPushed = 1;
        switch(buttonPushed) {
          case 1:   { print('matikan TV!'); break; }
          case 2:   { print('turunkan volume TV!'); break; }
          case 3:   { print('tingkatkan volume TV!'); break; }
          case 4:   { print('matikan suara TV!'); break; }
          default:  { print('Tidak terjadi apa-apa'); }
          }
    }