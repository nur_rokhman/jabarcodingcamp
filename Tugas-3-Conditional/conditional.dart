import 'dart:io';

void main(List<String> args) {
  // NOMOR 1 - ternary operator
        // print("apakah anda ingin menginstall aplikasi ini? (Y/T)");
        // String? inputInstallasi = stdin.readLineSync();
        // var isInstall = inputInstallasi;
        // String? output = (isInstall == "Y" || isInstall == "y") ? "anda akan menginstall aplikasi dart.. wait a minutes!"
        //           : (isInstall == "T" || isInstall == "t") ? "aborted.."
        //           : "Input Salah! hanya input huruf Y/y atau T/t saja yang diperbolehkan..";
        // print(output);


  // NOMOR 2 - IF, ELSE IF, ELSE
        // print("GAME WEREWOLF SEGERA DIMULAI!!!\n");
        // stdout.write("Nama : "); 
        // String? inputNama = stdin.readLineSync();
        // stdout.write("Peran : "); 
        // String? inputPeran = stdin.readLineSync();
        // var nama = inputNama.toString().trim();
        // var peran = inputPeran.toString().trim();
        
        // if(nama == "" && peran == ""){
        //   print("Nama harus diisi!");
        // } else if(nama == inputNama.toString().trim() && peran == ""){
        //   print("Pilih Peranmu untuk memulai game");
        // } else if(nama == "John" && peran == ""){
        //   print("Halo John, Pilih peranmu untuk memulai game!");
        // } else if(nama == "Jane" && peran == "Penyihir"){
        //   print("Selamat datang di Dunia Werewolf, Jane");
        //   print("Halo Penyihir Jane, kamu dapat melihat siapa yang menjadi werewolf!");
        // } else if (nama == "Jenita" && peran == "Guard"){
        //   print("Selamat datang di Dunia Werewolf, Jenita");
        //   print("Halo Guard Jenita, kamu akan membantu melindungi temanmu dari serangan werewolf.");
        // } else if(nama == "Junaedi" && peran == "Werewolf"){
        //   print("Selamat datang di Dunia Werewolf, Junaedi");
        //   print("Halo Werewolf Junaedi, Kamu akan memakan mangsa setiap malam!");
        // } else {
        //   print("Halo, selamat datang di dunia Werewolf!\n$nama, kamu berperan sebagai $peran");
        // }  
        
  
  // NOMOR 3 - SWITCH CASE QUOTES
        // stdout.write("Masukkan Hari (Senin - Minggu): ");
        // String? hari = stdin.readLineSync();
        // var pilihanHari = hari;
        // switch(pilihanHari) {
        //   case "Senin"  :   { print("Segala sesuatu memiliki kesudahan, yang sudah berakhir biarlah berlalu dan " +
        //                             "yakinlah semua akan baik-baik saja."); break; }
        //   case "Selasa" :   { print("Setiap detik sangatlah berharga karena waktu mengetahui banyak hal, termasuk " +
        //                             "rahasia hati."); break; }
        //   case "Rabu"   :   { print("Jika kamu tak menemukan buku yang kamu cari di rak, maka tulislah sendiri."); break; }
        //   case "Kamis"  :   { print("Jika hatimu banyak merasakan sakit, maka belajarlah dari rasa sakit itu untuk " +
        //                             " tidak memberikan rasa sakit pada orang lain."); break; }
        //   case "Jumat"  :   { print("Hidup tak selamanya tentang pacar."); break; }
        //   case "Sabtu"  :   { print("Rumah bukan hanya sebuah tempat, tetapi itu adalah perasaan."); break; }
        //   case "Minggu" :   { print("Hanya seseorang yang takut yang bisa bertindak berani. Tanpa rasa takut itu " +
        //                             "tidak ada apapun yang bisa disebut berani."); break; }
        //   default:  { print('Tidak ada quotes'); }
        //   }

  // SWITCH CASE, TANGGAL BULAN HARI
        var hari = 21; 
        var bulan = 1; 
        var tahun = 1945;
        switch(bulan) {
          case 1 :   { print("$hari Januari $tahun"); break; }
          case 2 :   { print("$hari Februari $tahun"); break; }
          case 3 :   { print("$hari Maret $tahun"); break; }
          case 4 :   { print("$hari April $tahun"); break; }
          case 5 :   { print("$hari Mei $tahun"); break; }
          case 6 :   { print("$hari Juni $tahun"); break; }
          case 7 :   { print("$hari Juli $tahun"); break; }
          case 8 :   { print("$hari Agustus $tahun"); break; }
          case 9 :   { print("$hari September $tahun"); break; }
          case 10:   { print("$hari Oktober $tahun"); break; }
          case 11:   { print("$hari November $tahun"); break; }
          case 12:   { print("$hari Desember $tahun"); break; }
          default:   { print('Masukkan Bulan yang benar!'); }
          } 
    }