import 'lingkaran.dart';
void main(List<String> args) {
  Lingkaran lingkaran = new Lingkaran();
  double? luasLingkaran;

  lingkaran.setRadius(6.0);
  luasLingkaran = lingkaran.hitungLuas();
  print(luasLingkaran.toStringAsFixed(2));
}
