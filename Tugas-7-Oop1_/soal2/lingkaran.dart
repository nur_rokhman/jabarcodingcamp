import 'dart:math';

class Lingkaran{
  double PI = pi; // double PI = 3.14159;
  double? radius;

  void setRadius(double value){
    if(value < 0){
      value *= -1; 
    }
    radius = value; //alias
  }
  
  double getRadius(){
    return radius!;
  }

  double hitungLuas() {
    return this.PI * pow(radius!, 2);
  }

}