// NOMOR 1
// void main(List<String> args) {
//     print(teriak()); // "Halo Sanbers!"
//   }

//   teriak(){
//     var cetak = "Halo Sanbers!";
//     return cetak;
//   }

// NOMOR 2
// void main(List<String> args) {
//   var num1 = 12;
//   var num2 = 4;
  
//   var hasilKali = nur_rokhman(num1, num2);
//   print(hasilKali); // 48
// }

// nur_rokhman(a, b){
//   return a*b;
// }

// NOMOR 3
// void main(List<String> args) {
//   var name = "Agus";
//   var age = 30;
//   var address = "Jln. Malioboro, Yogyakarta";
//   var hobby = "Gaming";

//   var perkenalan = introduce(name, age, address, hobby);
//   print(perkenalan);
// }

// introduce(a, b, c, d){
//   var cetak = "Nama saya $a, umur saya $b tahun, alamat saya di $c, dan saya punya hobby yaitu $d!";
//   return cetak; 
// }

// NOMOR 4
import 'dart:io';
    void main(List<String> args) {
      stdout.write("Masukkan angka : ");
      String? input = stdin.readLineSync();//instance class Scanner
      int angka = int.parse('$input');//new variable to save number
      var faktor = faktorial(angka);
      print("$angka! = $faktor");//call angka as parameter
    }

    faktorial(a) {
        if (a == 1 || a == 0)
            return 1;
        else
            return a * faktorial(a - 1);
    }